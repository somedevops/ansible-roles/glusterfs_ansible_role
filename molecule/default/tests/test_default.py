"""Role testing files using testinfra."""
import pytest
import testinfra

@pytest.mark.parametrize("pkgs", [
    "gnupg",
    "software-properties-common"
])
def test_installed_packages(host, pkgs):
    pkg = host.package(pkgs)
    assert pkg.is_installed

def test_glusterfs_service(host):
    s = host.service("glusterd")
    assert s.is_enabled
    assert s.is_running

def test_peer_status(host):
    peer_status = host.check_output('gluster peer status')
    assert 'Peer in Cluster (Connected)' in peer_status